from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_armies, diplo_print, check_support, support, move, check_friendly, status, evaluate, diplo_solve

#---------------
# Test Diplomacy
#---------------


class TestDiplomacy (TestCase):
	#----
	#read
	#----
	
	def test_read(self):
		s = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B']
		x = read_armies(s)
		dict = {'A': {'name': 'A', 'location': 'Madrid', 'action': 'Hold', 'object': ' ', 'friendly': '', 'og_location': 'Madrid', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'B': {'name': 'B', 'location': 'Barcelona', 'action': 'Move', 'object': 'Madrid', 'friendly': '', 'og_location': 'Barcelona', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'C': {'name': 'C', 'location': 'London', 'action': 'Support', 'object': 'B', 'friendly': '', 'og_location': 'London', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}}
		self.assertEqual(x, dict)
        
	def test_read2(self):
		s = ['A Madrid Move London', 'B London Move Madrid']
		x = read_armies(s)
		dict = {'A': {'name': 'A', 'location': 'Madrid', 'action': 'Move', 'object': 'London', 'friendly': '', 'og_location': 'Madrid', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'B': {'name': 'B', 'location': 'London', 'action': 'Move', 'object': 'Madrid', 'friendly': '', 'og_location': 'London', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}}
		self.assertEqual(x, dict)
    	
	def test_read3(self):
		s = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A']
		x = read_armies(s)
		dict = {'A': {'name': 'A', 'location': 'Madrid', 'action': 'Hold', 'object': ' ', 'friendly': '', 'og_location': 'Madrid', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'B': {'name': 'B', 'location': 'Barcelona', 'action': 'Move', 'object': 'Madrid', 'friendly': '', 'og_location': 'Barcelona', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'C': {'name': 'C', 'location': 'London', 'action': 'Move', 'object': 'Madrid', 'friendly': '', 'og_location': 'London', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'D': {'name': 'D', 'location': 'Paris', 'action': 'Support', 'object': 'B', 'friendly': '', 'og_location': 'Paris', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}, 'E': {'name': 'E', 'location': 'Austin', 'action': 'Support', 'object': 'A', 'friendly': '', 'og_location': 'Austin', 'ally_count': 1, 'enemy_count': 1, 'status': 'alive'}}
		self.assertEqual(x,dict)
    	

    #----
    #Evaluate
    #----
	def test_eval_1(self):
		lines = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B' ]
		x = read_armies(lines)
		v = evaluate(x)
		self.assertEqual(v, ['A [dead]', 'B Madrid', 'C London'])

	def test_eval_2(self):
		lines = ['A Madrid Move London', 'B London Move Madrid']
		x = read_armies(lines)
		v = evaluate(x)
		self.assertEqual(v, ['A London', 'B Madrid'])

	def test_eval_3(self):
		lines = ['A Madrid Hold','B Barcelona Move Madrid','C London Move Madrid','D Paris Support B', 'E Austin Support A']
		x = read_armies(lines)
		v = evaluate(x)
		self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]',  'E [dead]'])


    #----
    #Print
    #----

	def test_print1(self):
		w = StringIO()
		diplo_print(w, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'])
		self.assertEqual(w.getvalue(), 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
    
    
	def test_print2(self):
		w = StringIO()
		diplo_print(w, ['A Madrid Move London', 'B London Move Madrid'])
		self.assertEqual(w.getvalue(), 'A Madrid Move London\nB London Move Madrid\n')

	
	def test_print3(self):
		w = StringIO()
		diplo_print(w, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'])
		self.assertEqual(w.getvalue(), 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')


    #----
    #Solve
    #----

	def test_solve(self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
		w = StringIO()
		diplo_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')


	def test_solve2(self):
		r = StringIO('A Madrid Move London\nB London Move Madrid')
		w = StringIO()
		diplo_solve(r, w)
		self.assertEqual(w.getvalue(), 'A London\nB Madrid\n')

	def test_solve3(self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
		w = StringIO()
		diplo_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n')

# ----
# main
# ----
if __name__ == "__main__":
    main()
