#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# asya mazmanyan
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("A Miami Hold\nB Madrid Move LosAngeles\nC Austin Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Miami\nB LosAngeles\nC Austin\n")
    
    def test_solve_2(self):
        r = StringIO("D Miami Move KeyWest\nF KeyWest Move Miami\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "D KeyWest\nF Miami\n")
    
    def test_solve_3(self):
        r = StringIO("A Miami Hold\nB Madrid Move Miami\nC Austin Move Miami\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [DEAD]\nB [DEAD]\nC [DEAD]\n")
    

# ----
# main
# ----

if __name__ == "__main__":
    main()

