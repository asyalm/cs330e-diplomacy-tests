#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, engaged_with, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")

    # ----
    # engaged_with
    # ----

    def test_engaged_with(self):
        s = engaged_with(["A", "Madrid", "Mold"], ["A", "Madrid", "Mold"])
        self.assertEqual(s, None)

    # ----
    # eval
    # ----
    def test_eval_1(self):
        its = ["A", "Madrid", "Hold"]
        self.assertEqual(diplomacy_eval(its), [["A", "Madrid"]])

    def test_eval_2(self):
        its = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]]
        self.assertEqual(diplomacy_eval(
            *its), [["A", "[dead]"], ["B", "[dead]"]])

    def test_eval_3(self):
        its = [["A", "Madrid", "Hold"], ["B", "Barcelona",
                                         "Move", "Madrid"], ['C', "Austin", "Move", "Madrid"]]
        self.assertEqual(diplomacy_eval(
            *its), [["A", "[dead]"], ["B", "[dead]"], ['C', "[dead]"]])

    def test_eval_4(self):
        its = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona',
                                         'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        self.assertEqual(diplomacy_eval(
            *its), [["A", "[dead]"], ["B", "Madrid"], ["C", "London"]])


# # ----
# # print
# # ----

    def test_print_1(self):
        w = StringIO()
        eval_output = [["A", "Madrid"]]
        diplomacy_print(w, *eval_output)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        eval_output = [["A", "[dead]"], ["B", "[dead]"]]
        diplomacy_print(w, *eval_output)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        eval_output = [["A", "[dead]"], ["B", "[dead]"], ['C', "[dead]"]]
        diplomacy_print(w, *eval_output)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_6(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_8(self):
        r = StringIO(
            "A London Support B\nB Austin Support C\nC Houston Support D\nD Hamburg Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Houston\nD London\n")

        # ----
        # main
        # ----


if __name__ == "__main__":  # pragma: no cover
    main()  # pragma: no cover
