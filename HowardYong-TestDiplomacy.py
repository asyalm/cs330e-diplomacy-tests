#!/usr/bin/evn python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = 'A Madrid Hold\n'
        a = diplomacy_read(s)
        self.assertEqual(a, ['A', 'Madrid', 'Hold', None])
    
    def test_read_2(self):
        s = 'B Barcelona Move Madrid\n'
        a = diplomacy_read(s)
        self.assertEqual(a, ['B', 'Barcelona', 'Move', 'Madrid'])

    def test_read_3(self):
        s = 'E Austin Support A\n'
        a = diplomacy_read(s)
        self.assertEqual(a, ['E', 'Austin', 'Support', 'A'])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        events = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
            ['E', 'Austin', 'Support', 'A'],
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': '[dead]',
            'B': '[dead]',
            'C': '[dead]',
            'D': 'Paris',
            'E': 'Austin',
        })

    def test_eval_2(self):
        events = [
            ['A', 'Houston', 'Support', 'B'],
            ['B', 'Austin', 'Move', 'Houston']
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': '[dead]',
            'B': '[dead]'
        })
    
    def test_eval_3(self):
        events = [
            ['A', 'Houston', 'Move', 'Dallas'],
            ['B', 'Dallas', 'Move', 'Austin'],
            ['C', 'Austin', 'Move', 'Houston'],
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': 'Dallas',
            'B': 'Austin',
            'C': 'Houston'
        })

    def test_eval_4(self):
        events = [
            ['A', 'Houston', 'Move', 'Dallas'],
            ['B', 'Dallas', 'Move', 'Austin'],
            ['C', 'Austin', 'Move', 'Houston'],
            ['D', 'SanAntonio', 'Support', 'C']
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': 'Dallas',
            'B': 'Austin',
            'C': 'Houston',
            'D': 'SanAntonio'
        })

    def test_eval_5(self):
        events = [
            ['A', 'Madrid', 'Hold', None],
            ['B', 'London', 'Support', 'A'],
            ['C', 'Austin', 'Move', 'London'],
            ['D', 'NewYork', 'Move', 'Madrid']
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': '[dead]', 
            'B': '[dead]', 
            'C': '[dead]', 
            'D': '[dead]'
        })
    
    def test_eval_6(self):
        events = [
            ['A', 'Zurich', 'Move', 'Toronto'],
            ['B', 'Toronto', 'Move', 'Zurich']]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': 'Toronto', 
            'B': 'Zurich'
        })

    def test_eval_7(self):
        events = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'NewYork', 'Move','Madrid'],
            ['C', 'Boston', 'Support', 'B'],
            ['D', 'Chicago', 'Move', 'Boston'],
            ['E', 'SantaFe', 'Hold']
        ]
        v = diplomacy_eval(events)
        self.assertEqual(v, {
            'A': '[dead]', 
            'B': '[dead]', 
            'C': '[dead]', 
            'D': '[dead]', 
            'E': 'SantaFe'
        })

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        solution = {
            'A': '[dead]',
            'B': '[dead]',
            'C': '[dead]'
        }
        diplomacy_print(w, solution)
        self.assertEqual(w.getvalue(), 
            'A [dead]\nB [dead]\nC [dead]\n'
        )
    
    def test_print_2(self):
        w = StringIO()
        solution = {
            'A': 'Houston',
            'B': 'Austin',
            'C': 'Dallas'
        }
        diplomacy_print(w, solution)
        self.assertEqual(w.getvalue(), 
            'A Houston\nB Austin\nC Dallas\n'
        )
    
    def test_print_3(self):
        w = StringIO()
        solution = {
            'A': 'Austin',
            'B': '[dead]',
            'C': '[dead]'
        }
        diplomacy_print(w, solution)
        self.assertEqual(w.getvalue(), 
            'A Austin\nB [dead]\nC [dead]\n'
        )
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO('A Houston Support B\nB Austin Move Houston')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\n'
        )

    def test_solve_2(self):
        r = StringIO('A Houston Move Dallas\nB Dallas Move Austin\nC Austin Move Houston')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Dallas\nB Austin\nC Houston\n'
        )
    
    def test_solve_3(self):
        r = StringIO('A Houston Move Dallas\nB Dallas Move Austin\nC Austin Move Houston\nD SanAntonio Support C')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Dallas\nB Austin\nC Houston\nD SanAntonio\n'
        )

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          78     73     60      0     4%   14-17, 25-85, 93-94, 102-110, 117-130
TestDiplomacy.py      74     54      0      0    27%   22-24, 27-29, 32-34, 41-49, 58-63, 69-75, 82-89, 97-104, 112-116, 122-130, 143-150, 155-162, 167-174, 183-186, 191-194, 199-202
--------------------------------------------------------------
TOTAL                152    127     60      0    12%

"""
